module.exports = {
    init: function (appId, apiKey, successCallback) {
        cordova.exec(successCallback, null, "Airpush", "init", [appId, apiKey]);
    },
    showBanner360: function () {
        cordova.exec(null, null, "Airpush", "showBanner360", []);
    },
    showInterstitial: function () {
        cordova.exec(null, null, "Airpush", "showInterstitial", []);
    },
    startPushAd: function () {
        cordova.exec(null, null, "Airpush", "startPushAd", []);
    },
    startIconAd: function () {
        cordova.exec(null, null, "Airpush", "startIconAd", []);
    }
};